'use strict';
define(['choPidoTurnos'], function(choPidoTurnos) {
  choPidoTurnos.service('neighborhoodService', ['Restangular', function(Restangular) {
    this.getNeighborhoods = function(success) {
      Restangular.all('neighborhoods').getList().then(success);
    };
  }]);
});
