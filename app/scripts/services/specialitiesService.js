'use strict';

define(['choPidoTurnos'], function(choPidoTurnos) {
    choPidoTurnos.service('specialitiesService', ['Restangular', function(Restangular) {
      this.getSpecialities = function (success) {
        Restangular.all('specialities').getList().then(success);
      };
    }]);

});
