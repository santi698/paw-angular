'use strict';

define([], function() {
    return {
        defaultRoutePath: '/',
        routes: {
            '/': {
                templateUrl: '/views/index.html',
                controller: 'IndexCtrl'
            },
            '/about': {
                templateUrl: '/views/home.html',
                controller: 'HomeCtrl'
            }
            /* ===== yeoman hook ===== */
            /* Do not remove these commented lines! Needed for auto-generation */
        }
    };
});
