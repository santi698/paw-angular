'use strict';
define(['choPidoTurnos'], function(choPidoTurnos) {

	choPidoTurnos.directive('sample', function() {
		return {
			restrict: 'E',
			template: '<span>Sample</span>'
		};
	});
});
