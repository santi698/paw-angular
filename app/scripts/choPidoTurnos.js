'use strict';
define(['routes',
	'services/dependencyResolverFor',
	'i18n/i18nLoader!',
	'angular',
	'angular-route',
	'bootstrap',
	'angular-translate',
  'restangular'],
	function(config, dependencyResolverFor, i18n) {
		var choPidoTurnos = angular.module('choPidoTurnos', [
			'ngRoute',
			'pascalprecht.translate',
      'restangular'
		]);
		choPidoTurnos
			.config(
				['$routeProvider',
				'$controllerProvider',
				'$compileProvider',
				'$filterProvider',
				'$provide',
				'$translateProvider',
				function($routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $translateProvider) {
					choPidoTurnos.controller = $controllerProvider.register;
					choPidoTurnos.directive = $compileProvider.directive;
					choPidoTurnos.filter = $filterProvider.register;
					choPidoTurnos.factory = $provide.factory;
					choPidoTurnos.service = $provide.service;

					if (config.routes !== undefined) {
						angular.forEach(config.routes, function(route, path) {
							$routeProvider.when(path, {templateUrl: route.templateUrl, resolve: dependencyResolverFor(['controllers/' + route.controller]), controller: route.controller, gaPageTitle: route.gaPageTitle});
						});
					}
					if (config.defaultRoutePath !== undefined) {
						$routeProvider.otherwise({redirectTo: config.defaultRoutePath});
					}

					$translateProvider.translations('preferredLanguage', i18n);
					$translateProvider.preferredLanguage('preferredLanguage');
				}]);
    choPidoTurnos
      .config(
        ['RestangularProvider',
        function(RestangularProvider) {
          RestangularProvider.setBaseUrl('http://localhost:8080/grupo4');
          RestangularProvider.setResponseExtractor(
            function(data, operation) {
              if (operation === 'getList') {
                return data.list;
              }
              return data;
          });

        }]);
		return choPidoTurnos;
  }
);
