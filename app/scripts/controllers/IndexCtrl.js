'use strict';
define(['choPidoTurnos'], function(choPidoTurnos) {

  choPidoTurnos.controller('IndexCtrl', ['$scope', 'neighborhoodService', 'specialitiesService',
                  function($scope, neighborhoodService, specialitiesService) {
		$scope.welcomeText = 'Welcome to your test page';

    neighborhoodService.getNeighborhoods(function (neighborhoods) {
      $scope.neighborhoods = neighborhoods;
    });

    specialitiesService.getSpecialities(function (specialities) {
      $scope.specialities = specialities;
    });

	}]);
});
